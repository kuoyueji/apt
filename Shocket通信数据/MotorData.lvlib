﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="18008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Sever_MotorAction" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">控制电机指令枚举器.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/控制电机指令枚举器.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#9F!!!!"A!A!!!!!!"!))!]&gt;X!\8)!!!!"&amp;L`9VM;VZ\P[VLD"\M/WPNH'^SZD&gt;'Q!9U!7!!M%O&gt;C\_A/Z[4!%T;07O14,ZL;P",8)N0U)T;D1R='MP&gt;-)S^GWS,`9VM9)Q?0/O^#DV\Q)Q?0'L^#DV\Q)O`()I=[\VM-%S;D$[!!-P^D7RL8HO`L7O-(O!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_MotorMovePostion" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"A!A!!!!!!#!!V!#1!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"4;7ZH&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_MotorMoveVelocity" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"A!A!!!!!!#!!V!#1!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"4;7ZH&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_MotorReport" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">True</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">电机读取数据.ctl</Property>
		<Property Name="typedefName2" Type="Str">伺服状态.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/电机读取数据.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../../控件/伺服状态.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$"P1!!!"A!A!!!!!!&amp;!"&amp;!#A!+N&lt;((M,?^TLO^RQ!!'5!+!"/VM=?QN\X/O\X(S^GWS#_BYS^T!"&amp;!#A!+N&lt;((M,CJU@;^RQ!!'5!+!"/VM=?QO+H2^LX(S^GWS#_BYS^T!$E!]1!!!!!!!!!"%,8HO`KWQ=CBSPW_X3ZD&gt;'Q!)%"1!!1!!!!"!!)!!QSVZ\P[NM()I=L^PNU!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_MotorSource" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">电机资源.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/电机资源.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"\&gt;Q!!!"A!A!!!!!!#!"Y!.`````]!""A!A!!!!!!"!!1!!!!"!!!!!!!!!%5!]1!!!!!!!!!"$,8HO`L8SN3U,G.U&lt;!!Q1(!!$A6*&lt;H.U=A!"!!!9!)!!!!!!!1!%!!!!!1!!!!!!!!?VZ\P[1U^.!!%!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_TrackingBoardAction" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">精跟踪指令.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/精跟踪指令.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#LJQ!!!"A!A!!!!!!"!*9!]1!!!!!!!!!"$L[LO0L8W&gt;;YQ?YO9X2M!(^!&amp;1!*",8)N0U,5&amp;.%TLP7Q\&lt;"S+%/P_SXN,[VTLP7Q\`9VM9/P_SXN,[VTLP7Q\&lt;"S+%1R^#\O^$&amp;M?KZYL?CS;+^RR#`KLOXM&gt;7\N]3DSLW`W.&lt;'$,(6O\@/O^&lt;$N\4!I14"N,X4",H9M&gt;5!#L[LO0L8W&gt;;YQ?Y!!!%!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_TrackingBoardLight" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">信标光光源.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/信标光光源.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"651!!!"A!A!!!!!!"!%!!]1!!!!!!!!!"$N$&amp;M?KZYLHCV,1O9X2M!#F!&amp;1!##,4TN[,*ILX(#.#BN[,*ILX(!!L1R&lt;(KO?+ZYN3U!!!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_TrackingBoardOpenLoop" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">精跟踪模式.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/精跟踪模式.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!".31!!!"A!A!!!!!!"!$A!]1!!!!!!!!!"$L[LO0L8W=3DSLUO9X2M!#&amp;!&amp;1!#",_KO\=%M&gt;7\NQ!+PKOY_N@:R+0+P1!!!1!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_TrackingBoardPZTPosition" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""01!!!"A!A!!!!!!#!!V!#1!(4H6N:8*J9Q!=1%!!!@````]!!!^"=H*B?3"P:C"4;7ZH&lt;'5!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_TrackingBoardReport" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">精跟踪状态.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/精跟踪状态.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#GIA!!!"A!A!!!!!!'!!^!"A!)5&amp;.%7.@YM?I!!!^!"A!)5&amp;.%7&gt;@YM?I!!!^!"A!*P_SXN,[V7.&lt;B!!^!"A!*P_SXN,[V7&gt;&lt;B!"*!)1SRV&lt;OXML&lt;8P&gt;?UT+Q!!$M!]1!!!!!!!!!"$L[LO0L8W&gt;?UT+QO9X2M!#2!5!!&amp;!!!!!1!#!!-!"![_K\D[V^H5S^$1V\4-L!!!!1!&amp;!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_TrackingBoardSource" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typedefName1" Type="Str">电机资源.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../控件/电机资源.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"\&gt;Q!!!"A!A!!!!!!#!"Y!.`````]!""A!A!!!!!!"!!1!!!!"!!!!!!!!!%5!]1!!!!!!!!!"$,8HO`L8SN3U,G.U&lt;!!Q1(!!$A6*&lt;H.U=A!"!!!9!)!!!!!!!1!%!!!!!1!!!!!!!!?VZ\P[1U^.!!%!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Sever_WriteFile" Type="Variable">
		<Property Name="featurePacks" Type="Str">Global</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Global</Property>
		<Property Name="typeDesc" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"A!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
