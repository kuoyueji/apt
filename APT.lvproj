﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Property Name="varPersistentID:{1149A5B3-ADBD-4D17-A6F0-219EF5605A8B}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_TrackingBoardPZTPosition</Property>
	<Property Name="varPersistentID:{33F231D5-932A-4C43-B807-34A8E0B210FE}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_TrackingBoardLight</Property>
	<Property Name="varPersistentID:{555EEBF5-A52C-4DA5-83B7-819188EC512C}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_TrackingBoardReport</Property>
	<Property Name="varPersistentID:{639BC6C6-ED5D-4159-BEC0-24BD7C644BFC}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_MotorReport</Property>
	<Property Name="varPersistentID:{66A503C6-0431-43C8-85D6-7A8159F5B728}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_TrackingBoardOpenLoop</Property>
	<Property Name="varPersistentID:{81F7C4BF-35D0-4369-AD85-6E9E7F3750EE}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_MotorMovePostion</Property>
	<Property Name="varPersistentID:{848BB81A-DDAA-49DF-975F-A9BE8C1107D8}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_TrackingBoardSource</Property>
	<Property Name="varPersistentID:{87F97CFE-12D0-4AA2-B5CA-135BCD1714DF}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_TrackingBoardAction</Property>
	<Property Name="varPersistentID:{A8BC313C-037E-4F11-8898-714E176EB596}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_MotorSource</Property>
	<Property Name="varPersistentID:{D6F3F5FC-D4B8-4DE1-AB97-BD9F54841185}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_MotorMoveVelocity</Property>
	<Property Name="varPersistentID:{EED8FB44-84AC-4478-9A6D-E3E21F4E594C}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_WriteFile</Property>
	<Property Name="varPersistentID:{F267A916-D063-412C-A921-5E466072C2AC}" Type="Ref">/我的电脑/通用/MotorData.lvlib/Sever_MotorAction</Property>
	<Item Name="我的电脑" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">我的电脑/VI服务器</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">我的电脑/VI服务器</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="FTP" Type="Folder">
			<Item Name="FTPFileChangeDelte.vi" Type="VI" URL="../FTP/FTPFileChangeDelte.vi"/>
			<Item Name="FTPFileChangeRead.vi" Type="VI" URL="../FTP/FTPFileChangeRead.vi"/>
			<Item Name="FTPFileChangeWrite.vi" Type="VI" URL="../FTP/FTPFileChangeWrite.vi"/>
		</Item>
		<Item Name="电机" Type="Folder">
			<Item Name="A1Frame.vi" Type="VI" URL="../电机/A1Frame.vi"/>
			<Item Name="A2Frame.vi" Type="VI" URL="../电机/A2Frame.vi"/>
			<Item Name="B1Frame.vi" Type="VI" URL="../电机/B1Frame.vi"/>
			<Item Name="B2Frame.vi" Type="VI" URL="../电机/B2Frame.vi"/>
			<Item Name="ControlMotor.vi" Type="VI" URL="../电机/ControlMotor.vi"/>
			<Item Name="DataToFrame.vi" Type="VI" URL="../电机/DataToFrame.vi"/>
			<Item Name="GetMotorVelocity.vi" Type="VI" URL="../电机/GetMotorVelocity.vi"/>
			<Item Name="ReceiveDataFrame.vi" Type="VI" URL="../电机/ReceiveDataFrame.vi"/>
			<Item Name="SendDataFrame.vi" Type="VI" URL="../电机/SendDataFrame.vi"/>
			<Item Name="Xor8.vi" Type="VI" URL="../电机/Xor8.vi"/>
			<Item Name="GetMotorPositionVel.vi" Type="VI" URL="../电机/GetMotorPositionVel.vi"/>
			<Item Name="GetMotorPosition.vi" Type="VI" URL="../电机/GetMotorPosition.vi"/>
			<Item Name="GetMotorState.vi" Type="VI" URL="../电机/GetMotorState.vi"/>
			<Item Name="ParseMotorPositionVel.vi" Type="VI" URL="../电机/ParseMotorPositionVel.vi"/>
			<Item Name="MotorPitchScan.vi" Type="VI" URL="../电机/MotorPitchScan.vi"/>
			<Item Name="MotorAzimuthScan.vi" Type="VI" URL="../电机/MotorAzimuthScan.vi"/>
			<Item Name="MotorMoveZero.vi" Type="VI" URL="../电机/MotorMoveZero.vi"/>
			<Item Name="MotorVelocityMode.vi" Type="VI" URL="../电机/MotorVelocityMode.vi"/>
			<Item Name="MotorStop.vi" Type="VI" URL="../电机/MotorStop.vi"/>
			<Item Name="MotorMovePosition.vi" Type="VI" URL="../电机/MotorMovePosition.vi"/>
			<Item Name="MotorConnet.vi" Type="VI" URL="../电机/MotorConnet.vi"/>
			<Item Name="MotorClose.vi" Type="VI" URL="../电机/MotorClose.vi"/>
			<Item Name="Chage360To180.vi" Type="VI" URL="../电机/Chage360To180.vi"/>
			<Item Name="MotorScanHandle.vi" Type="VI" URL="../电机/MotorScanHandle.vi"/>
			<Item Name="MotorTest.vi" Type="VI" URL="../电机/MotorTest.vi"/>
			<Item Name="MotorUpdatePos.vi" Type="VI" URL="../电机/MotorUpdatePos.vi"/>
			<Item Name="MotorWaitUpdateLight.vi" Type="VI" URL="../电机/MotorWaitUpdateLight.vi"/>
			<Item Name="MotorMoveRef.vi" Type="VI" URL="../电机/MotorMoveRef.vi"/>
			<Item Name="TrackInPositionMode.vi" Type="VI" URL="../电机/TrackInPositionMode.vi"/>
			<Item Name="MotorStateMachine.vi" Type="VI" URL="../电机/MotorStateMachine.vi"/>
		</Item>
		<Item Name="参数获取" Type="Folder">
			<Item Name="ParameterReadCluster.vi" Type="VI" URL="../参数获取/ParameterReadCluster.vi"/>
			<Item Name="ParameterWriteCluster.vi" Type="VI" URL="../参数获取/ParameterWriteCluster.vi"/>
			<Item Name="GetCameraParameter.vi" Type="VI" URL="../参数获取/GetCameraParameter.vi"/>
			<Item Name="GetPreciseTrackingBoardParameter.vi" Type="VI" URL="../参数获取/GetPreciseTrackingBoardParameter.vi"/>
			<Item Name="GetShakingPlatformParameter.vi" Type="VI" URL="../参数获取/GetShakingPlatformParameter.vi"/>
			<Item Name="GetBitErrorTestParameter.vi" Type="VI" URL="../参数获取/GetBitErrorTestParameter.vi"/>
			<Item Name="GetDataNetworkParameter.vi" Type="VI" URL="../参数获取/GetDataNetworkParameter.vi"/>
			<Item Name="GetControlNetworkParameter.vi" Type="VI" URL="../参数获取/GetControlNetworkParameter.vi"/>
			<Item Name="GetMotorParameter.vi" Type="VI" URL="../参数获取/GetMotorParameter.vi"/>
			<Item Name="GetLogFilePatameter.vi" Type="VI" URL="../参数获取/GetLogFilePatameter.vi"/>
			<Item Name="WriteCameraParameter.vi" Type="VI" URL="../参数获取/WriteCameraParameter.vi"/>
			<Item Name="WritePreciseTrackingBoardParameter.vi" Type="VI" URL="../参数获取/WritePreciseTrackingBoardParameter.vi"/>
			<Item Name="WriteMotorParameter.vi" Type="VI" URL="../参数获取/WriteMotorParameter.vi"/>
			<Item Name="WriteDataNetworkParameter.vi" Type="VI" URL="../参数获取/WriteDataNetworkParameter.vi"/>
			<Item Name="WriteLogFilePatameter.vi" Type="VI" URL="../参数获取/WriteLogFilePatameter.vi"/>
			<Item Name="WriteControlNetworkParameter.vi" Type="VI" URL="../参数获取/WriteControlNetworkParameter.vi"/>
		</Item>
		<Item Name="控件" Type="Folder" URL="../控件">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="精跟踪" Type="Folder" URL="../精跟踪">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="全局变量" Type="Folder">
			<Item Name="电机状态消息.vi" Type="VI" URL="../全局变量/电机状态消息.vi"/>
			<Item Name="Message.vi" Type="VI" URL="../全局变量/Message.vi"/>
			<Item Name="精跟踪状态消息.vi" Type="VI" URL="../全局变量/精跟踪状态消息.vi"/>
			<Item Name="ReceiveDataCheck.vi" Type="VI" URL="../全局变量/ReceiveDataCheck.vi"/>
			<Item Name="相机状态消息.vi" Type="VI" URL="../全局变量/相机状态消息.vi"/>
		</Item>
		<Item Name="相机" Type="Folder" URL="../相机">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="PID" Type="Folder" URL="../PID">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="通用" Type="Folder">
			<Item Name="MotorData.lvlib" Type="Library" URL="../Shocket通信数据/MotorData.lvlib"/>
			<Item Name="SerialPortToVISA.vi" Type="VI" URL="../SerialPortToVISA.vi"/>
			<Item Name="SetCtlsData.vi" Type="VI" URL="../SetCtlsData.vi"/>
			<Item Name="PanelInit.vi" Type="VI" URL="../PanelInit.vi"/>
			<Item Name="DialogForSelectFileName.vi" Type="VI" URL="../FTP/DialogForSelectFileName.vi"/>
			<Item Name="UserErrorHandle.vi" Type="VI" URL="../电机/UserErrorHandle.vi"/>
			<Item Name="TestCenter.vi" Type="VI" URL="../TestCenter.vi"/>
			<Item Name="SoftWareStop.vi" Type="VI" URL="../SoftWareStop.vi"/>
			<Item Name="OpenLogFile.vi" Type="VI" URL="../通用/OpenLogFile.vi"/>
			<Item Name="CloseLogFile.vi" Type="VI" URL="../通用/CloseLogFile.vi"/>
			<Item Name="ShowLogFile.vi" Type="VI" URL="../通用/ShowLogFile.vi"/>
			<Item Name="SelectFileByInputName.vi" Type="VI" URL="../通用/SelectFileByInputName.vi"/>
			<Item Name="FalseStateSwitch.vi" Type="VI" URL="../通用/FalseStateSwitch.vi"/>
		</Item>
		<Item Name="Network" Type="Folder">
			<Item Name="ControlNetwork" Type="Folder">
				<Item Name="ControlNetworkClientSendData.vi" Type="VI" URL="../网络/ControlNetworkClientSendData.vi"/>
				<Item Name="ControlNetworkClientQueryData.vi" Type="VI" URL="../网络/ControlNetworkClientQueryData.vi"/>
				<Item Name="ControlNetworkCheckConnet.vi" Type="VI" URL="../网络/ControlNetworkCheckConnet.vi"/>
				<Item Name="ControlNetworkGetInitState.vi" Type="VI" URL="../网络/ControlNetworkGetInitState.vi"/>
				<Item Name="ControlNetworkSendRunningState.vi" Type="VI" URL="../网络/ControlNetworkSendRunningState.vi"/>
				<Item Name="ControlNetworkSetInitState.vi" Type="VI" URL="../网络/ControlNetworkSetInitState.vi"/>
				<Item Name="ControlNetworkSendTaskStart.vi" Type="VI" URL="../网络/ControlNetworkSendTaskStart.vi"/>
				<Item Name="ControlNetworkSwitchLight.vi" Type="VI" URL="../网络/ControlNetworkSwitchLight.vi"/>
				<Item Name="ControlNetworkSever.vi" Type="VI" URL="../网络/ControlNetworkSever.vi"/>
				<Item Name="ControlNetworkSendStop.vi" Type="VI" URL="../网络/ControlNetworkSendStop.vi"/>
				<Item Name="ControlNetworkSendBitErrorRate.vi" Type="VI" URL="../网络/ControlNetworkSendBitErrorRate.vi"/>
			</Item>
			<Item Name="DataNetwork" Type="Folder">
				<Item Name="DataNetworkSever.vi" Type="VI" URL="../网络/DataNetworkSever.vi"/>
				<Item Name="DataNetworkFTPReceive.vi" Type="VI" URL="../网络/DataNetworkFTPReceive.vi"/>
				<Item Name="DataNetworkClientSendData.vi" Type="VI" URL="../网络/DataNetworkClientSendData.vi"/>
				<Item Name="DataNetworkTestDataSave.vi" Type="VI" URL="../网络/DataNetworkTestDataSave.vi"/>
				<Item Name="DataNetworkSendFile.vi" Type="VI" URL="../网络/DataNetworkSendFile.vi"/>
				<Item Name="DataNetworkFTPSend.vi" Type="VI" URL="../网络/DataNetworkFTPSend.vi"/>
				<Item Name="DataNetworkSendTestData.vi" Type="VI" URL="../网络/DataNetworkSendTestData.vi"/>
				<Item Name="DataNetworkTestHeartBeat.vi" Type="VI" URL="../网络/DataNetworkTestHeartBeat.vi"/>
				<Item Name="DataNetworkClientQueryData.vi" Type="VI" URL="../网络/DataNetworkClientQueryData.vi"/>
				<Item Name="GenerateTestData.vi" Type="VI" URL="../网络/GenerateTestData.vi"/>
				<Item Name="ReceiveTestDataCheck.vi" Type="VI" URL="../网络/ReceiveTestDataCheck.vi"/>
				<Item Name="GetTestDataSomeRow.vi" Type="VI" URL="../网络/GetTestDataSomeRow.vi"/>
				<Item Name="GetTestDataOneRow.vi" Type="VI" URL="../网络/GetTestDataOneRow.vi"/>
			</Item>
			<Item Name="STMParseData.vi" Type="VI" URL="../网络/STMParseData.vi"/>
			<Item Name="STMSendData.vi" Type="VI" URL="../网络/STMSendData.vi"/>
		</Item>
		<Item Name="震动平台" Type="Folder">
			<Item Name="StartShockPlatform.vi" Type="VI" URL="../震动平台/StartShockPlatform.vi"/>
			<Item Name="StartFileUsingCMD.vi" Type="VI" URL="../震动平台/StartFileUsingCMD.vi"/>
		</Item>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="Parameter.ini" Type="Document" URL="../Parameter.ini"/>
		<Item Name="ControlNetworkSendMotorRotate.vi" Type="VI" URL="../网络/ControlNetworkSendMotorRotate.vi"/>
		<Item Name="依赖关系" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
				<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Encode Section and Key Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Encode Section and Key Names__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Strip Units__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Strip Units__ogtk.vi"/>
				<Item Name="Array Size(s)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array Size(s)__ogtk.vi"/>
				<Item Name="Get Array Element TDEnum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element TDEnum__ogtk.vi"/>
				<Item Name="Reshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Reshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="Get Waveform Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Waveform Type Enum from TD__ogtk.vi"/>
				<Item Name="Waveform Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Waveform Subtype Enum__ogtk.ctl"/>
				<Item Name="Array to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array to Array of VData__ogtk.vi"/>
				<Item Name="Format Numeric Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Format Numeric Array__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Resolve Timestamp Format__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Resolve Timestamp Format__ogtk.vi"/>
				<Item Name="Get Waveform Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Waveform Type Enum from Data__ogtk.vi"/>
				<Item Name="Refnum Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Refnum Subtype Enum__ogtk.ctl"/>
				<Item Name="Get Refnum Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Refnum Type Enum from TD__ogtk.vi"/>
				<Item Name="Get Refnum Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Refnum Type Enum from Data__ogtk.vi"/>
				<Item Name="Format Variant Into String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Format Variant Into String__ogtk.vi"/>
				<Item Name="Write Key (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Write Key (Variant)__ogtk.vi"/>
				<Item Name="Write Section Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Write Section Cluster__ogtk.vi"/>
				<Item Name="Write INI Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Write INI Cluster__ogtk.vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Get Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Element TD from Array TD__ogtk.vi"/>
				<Item Name="Get Array Element TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element TD__ogtk.vi"/>
				<Item Name="Reshape 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Reshape 1D Array__ogtk.vi"/>
				<Item Name="Array of VData to VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VArray__ogtk.vi"/>
				<Item Name="Get Default Data from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Default Data from TD__ogtk.vi"/>
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Read Key (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Read Key (Variant)__ogtk.vi"/>
				<Item Name="Read Section Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Read Section Cluster__ogtk.vi"/>
				<Item Name="Read INI Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Read INI Cluster__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Panel.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Close Panel.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Create Semaphore.vi"/>
				<Item Name="Destroy Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Destroy Semaphore.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get Semaphore Status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Get Semaphore Status.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_FTP.lvlib" Type="Library" URL="/&lt;vilib&gt;/FTP/NI_FTP.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Panel.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Open Panel.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Semaphore Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Name &amp; Ref DB Action.ctl"/>
				<Item Name="Semaphore Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Name &amp; Ref DB.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="NI_Vision_Acquisition_Software.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/driver/NI_Vision_Acquisition_Software.lvlib"/>
				<Item Name="IMAQdx.ctl" Type="VI" URL="/&lt;vilib&gt;/userdefined/High Color/IMAQdx.ctl"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="Imaq Dispose" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/Imaq Dispose"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="Error to Warning.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error to Warning.vi"/>
				<Item Name="NI STM.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI/STM/NI STM.lvlib"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="VISA Set IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Set IO Buffer Mask.ctl"/>
				<Item Name="IMAQ ReadFile" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ ReadFile"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="IMAQ Overlay Rectangle" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Rectangle"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Get Waveform Time Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get Waveform Time Array.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="WDT Get Waveform Time Array DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Time Array DBL.vi"/>
				<Item Name="Get Waveform Time Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get Waveform Time Array.vi"/>
				<Item Name="compatWriteText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatWriteText.vi"/>
				<Item Name="Write File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write File+ (string).vi"/>
				<Item Name="Waveform Time to Date Time String.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Waveform Time to Date Time String.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Export Waveforms To Spreadsheet File (Digital).vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Export Waveforms To Spreadsheet File (Digital).vi"/>
				<Item Name="Export Waveform To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Export Waveform To Spreadsheet File.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Export Waveforms To Spreadsheet File (2D).vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Export Waveforms To Spreadsheet File (2D).vi"/>
				<Item Name="Export Waveforms To Spreadsheet File (1D).vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Export Waveforms To Spreadsheet File (1D).vi"/>
				<Item Name="Export Waveforms to Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Export Waveforms to Spreadsheet File.vi"/>
			</Item>
			<Item Name="niimaqdx.dll" Type="Document" URL="niimaqdx.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="PowerProcess.vi" Type="VI" URL="../PowerProcess.vi"/>
			<Item Name="TLPM Get Resource Name.vi" Type="VI" URL="../TLPM/TLPM.llb/TLPM Get Resource Name.vi"/>
			<Item Name="TLPM Initialize.vi" Type="VI" URL="../TLPM/TLPM.llb/TLPM Initialize.vi"/>
			<Item Name="TLPM Measure Power.vi" Type="VI" URL="../TLPM/TLPM.llb/TLPM Measure Power.vi"/>
			<Item Name="TLPM Close.vi" Type="VI" URL="../TLPM/TLPM.llb/TLPM Close.vi"/>
			<Item Name="TLPM Find Resources.vi" Type="VI" URL="../TLPM/TLPM.llb/TLPM Find Resources.vi"/>
			<Item Name="TLPM VXIpnp Error Converter.vi" Type="VI" URL="../TLPM/TLPM.llb/TLPM VXIpnp Error Converter.vi"/>
			<Item Name="TLPM_32.dll" Type="Document" URL="TLPM_32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="loadSubVI.vi" Type="VI" URL="../Shocket通信数据/loadSubVI.vi"/>
		</Item>
		<Item Name="程序生成规范" Type="Build"/>
	</Item>
</Project>
