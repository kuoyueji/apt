﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Item Name="我的电脑" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">我的电脑/VI服务器</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">我的电脑/VI服务器</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="libVLCclose.vi" Type="VI" URL="../libVLCclose.vi"/>
		<Item Name="libVLCinit.vi" Type="VI" URL="../libVLCinit.vi"/>
		<Item Name="libVLCpause.vi" Type="VI" URL="../libVLCpause.vi"/>
		<Item Name="libVLCplay.vi" Type="VI" URL="../libVLCplay.vi"/>
		<Item Name="libVLCsetPos.vi" Type="VI" URL="../libVLCsetPos.vi"/>
		<Item Name="VLCtest.vi" Type="VI" URL="../VLCtest.vi"/>
		<Item Name="依赖关系" Type="Dependencies">
			<Item Name="libvlc.dll" Type="Document" URL="/C/Program Files (x86)/VideoLAN/VLC/libvlc.dll"/>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System.Windows.Forms" Type="Document" URL="System.Windows.Forms">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="程序生成规范" Type="Build"/>
	</Item>
</Project>
